# Establishing a connection to a MongoDB Atlas cluster using pymongo.

# Importing the MongoClient class from the pymongo library.
from pymongo import MongoClient

# Connecting to the MongoDB Atlas cluster using the MongoClient constructor and providing the connection string.
client = MongoClient("mongodb+srv://seth8806:Jxb6uAHmzk1ubzRR@cluster.ct2tktb.mongodb.net/?retryWrites=true&w=majority")

# Accessing the 'test' database within the connected cluster.
db = client.sample_mflix

# Accessing the 'users' collection within the 'test' database.
user_collection = db["users"]

# Accessing the 'theaters' collection within the 'test' database.
theaters_collection = db["theaters"]

# Accessing the 'movies' collection within the 'test' database.
movies_collection = db["movies"]